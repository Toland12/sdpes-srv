var ApiError = require('../app/error/ApiError');

/**
 * 在app.use(router)之前调用
 */
var response_formatter = (ctx) => {

    //如果有返回数据，将返回数据添加到data中
    if (ctx.body) {
        ctx.body = {
            code: 0,
            message: 'success',
            data: ctx.body
        }
    } else {
        ctx.body = {
            code: 0,
            message: 'success'
        }
    }
}

var check_session = (ctx) => {

    //判断有无session，没有则直接返回
    
    let url_arr = ctx.url.split('/')

    console.log(url_arr[url_arr.length - 1])
    if(url_arr[url_arr.length - 1] !== 'login'){
        /*console.log(ctx.session.user)*/
        if(!ctx.session.user){
            ctx.body = {
                code: 3,
                message: '登录状态已过期，请重新登录'
            }
            return false;
        }
    }
    return true;
}

var url_filter = (pattern) => {

    return async (ctx, next) => {
        if(!check_session(ctx))
            return;

        var reg = new RegExp(pattern);
        try {
            //先去执行路由
            await next();
        } catch (error) {
            //如果异常类型是API异常并且通过正则验证的url，将错误信息添加到响应体中返回。
            if(error instanceof ApiError && reg.test(ctx.originalUrl)){
                ctx.status = 200;
                ctx.body = {
                    code: error.code,
                    message: error.message
                }
            }
            //继续抛，让外层中间件处理日志
            throw error;
        }
        
        //通过正则的url进行格式化处理
        if(reg.test(ctx.originalUrl)){

            response_formatter(ctx);
        }
    }
}

// module.exports = response_formatter;
module.exports = url_filter;