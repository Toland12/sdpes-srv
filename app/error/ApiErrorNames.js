/**
 * API错误名称
 */
var ApiErrorNames = {};

ApiErrorNames.UNKNOW_ERROR = "unknowError";
ApiErrorNames.USER_NOT_EXIST = "userNotExist";
ApiErrorNames.PHONENUMBER_EXIST = "phoneNumberExist"
ApiErrorNames.LOGIN_OUTTIME = 'loginOutTime'
ApiErrorNames.AUTH_FAILED = 'authFailed'

/* people */
ApiErrorNames.SAVE_PEOPLE_FAILED = 'savePeopleFailed'
ApiErrorNames.DELETE_PEOPLE_FAILED = 'deletePeopleFailed'

/* schooldata */
ApiErrorNames.SAVE_SCHOOLDATA_FAILED = 'saveSchooldataFailed'
ApiErrorNames.AUDIT_FAILED = 'auditFailed'

/**
 * API错误名称对应的错误信息
 */
const error_map = new Map();

error_map.set(ApiErrorNames.UNKNOW_ERROR, { code: -1, message: '未知错误' });
error_map.set(ApiErrorNames.USER_NOT_EXIST, { code: 101, message: '用户不存在' });
error_map.set(ApiErrorNames.PHONENUMBER_EXIST, { code: 102, message: '手机号已存在' });
error_map.set(ApiErrorNames.LOGIN_OUTTIME, { code: 103, message: '登录状态已过期' });

error_map.set(ApiErrorNames.SAVE_PEOPLE_FAILED, { code: 201, message: '保存人员信息失败' });
error_map.set(ApiErrorNames.DELETE_PEOPLE_FAILED, { code: 202, message: '删除人员信息失败' });

error_map.set(ApiErrorNames.SAVE_SCHOOLDATA_FAILED, { code: 301, message: '保存学校数据失败' });
error_map.set(ApiErrorNames.AUDIT_FAILED, { code: 302, message: '审核学校数据失败' });


//根据错误名称获取错误信息
ApiErrorNames.getErrorInfo = (error_name) => {

    var error_info;

    if (error_name) {
        error_info = error_map.get(error_name);
    }

    //如果没有对应的错误信息，默认'未知错误'
    if (!error_info) {
        error_name = ApiErrorNames.UNKNOW_ERROR;
        error_info = error_map.get(error_name);
    }
    
    return error_info;
}

module.exports = ApiErrorNames;