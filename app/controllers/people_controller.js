const ApiError      = require('../error/ApiError');
const ApiErrorNames = require('../error/ApiErrorNames');

const People        = require('../model/people_model');


exports.list = async (ctx, next) => {
    let userid = ctx.session.user.id

    let result = await People.list(userid)
    if(result.length)
        ctx.body = result
    else
        ctx.body = []
}


exports.save = async (ctx, next) => {
    let people = ctx.request.body
    people.userid = ctx.session.user.id
    let result = null
    if(people.id)
        result = await People.save(people)
    else
        result = await People.add(people)
    if(result)
        ctx.body = result
    else
        throw new ApiError(ApiErrorNames.SAVE_PEOPLE_FAILED);
}


exports.delete = async (ctx, next) => {
    let id = ctx.request.query.id;
    let result = await People.delete(id)
    if(result)
        ctx.body = result
    else
        throw new ApiError(ApiErrorNames.DELETE_PEOPLE_FAILED);
}