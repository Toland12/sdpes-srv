const db = require('./../db')

const TABLE = 'sd_people'

const people = {


  async list (userid) {
    let _sql = `
      SELECT * from sd_people
      where userid="${userid}" and delete_flag = 0
    `
    let result = await db.query(_sql)

    return result
  },

  async add(people){
    let result = await db.insertData(TABLE, people)
    return result
  },

  async save(people){
    let result = await db.updateData(TABLE, people, people.id)
    return result
  },

  async delete(id){
    let result = await db.deleteDataById(TABLE, id)
    return result
  }
}

module.exports = people
