var router = require('koa-router')();
var people_controller = require('../../app/controllers/people_controller');

router.post('/save', people_controller.save);
router.delete('/delete', people_controller.delete);
router.get('/list', people_controller.list);

module.exports = router;
