var router = require('koa-router')();
var user_controller = require('../../app/controllers/user_controller');

router.post('/login', user_controller.login);
router.post('/signup', user_controller.signup);
router.get('/logout', user_controller.logout)

module.exports = router;
