var router = require('koa-router')()
var user_router = require('./user_router')
var people_router = require('./people_router')
var schooldata_router = require('./schooldata_router')
var notice_router = require('./notice_router')

router.use('/user', user_router.routes(), user_router.allowedMethods())
router.use('/people', people_router.routes(), people_router.allowedMethods())
router.use('/notice', notice_router.routes(), notice_router.allowedMethods())
router.use('/schooldata', schooldata_router.routes(), schooldata_router.allowedMethods())

module.exports = router;
