const ApiError      = require('../error/ApiError');
const ApiErrorNames = require('../error/ApiErrorNames');

const User          = require('../model/user_model'); 


exports.login = async (ctx, next) => {

    let username = ctx.request.body.username.trim();
    let password = ctx.request.body.password.trim();
    
    let user = await User.getOneByUserNameAndPassword({
        username: username,
        password: password
    });

    if(user){
        ctx.body = {
            id: user.id,
            username: user.username,
            name: user.name,
            role: user.user_type,
            schoolType: user.school_type
        }
        ctx.session.user = user;
    }
    else
        throw new ApiError(ApiErrorNames.USER_NOT_EXIST);
}


exports.signup = async (ctx, next) => {
    
}


exports.logout = async (ctx, next) => {
    ctx.session.user = null;
    ctx.body = [];
}