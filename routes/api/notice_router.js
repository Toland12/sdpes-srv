var router = require('koa-router')();
var notice_controller = require('../../app/controllers/notice_controller');

router.post('/announcement', notice_controller.announcement);
router.get('/announcement', notice_controller.announcementList);

router.post('/message', notice_controller.message);
router.get('/message', notice_controller.messageList);

router.get('/getuser', notice_controller.getPushUser);

module.exports = router;
