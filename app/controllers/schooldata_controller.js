const ApiError      = require('../error/ApiError')
const ApiErrorNames = require('../error/ApiErrorNames')

const SchoolData    = require('../model/schooldata_model')
const NoticeCon     = require('./notice_controller')

const xlsx          = require('node-xlsx').default
const fs            = require('fs')
const sendFile      = require('koa-send')


/* File Path */
var path = require('path');
//const OUTPUTEXLSPATH = path.resolve(__dirname, '../../statics') + '/outputEXLS/'
const OUTPUTEXLSPATH = path.resolve('/huizhi/developer/filerepo/tpdsp/file/upload')


exports.one = async (ctx, next) => {
    let userid = ctx.session.user.id

    let result = await SchoolData.one(userid)
    if(result.length > 0)
        ctx.body = result[0]
    else
        ctx.body = []
}


exports.save = async (ctx, next) => {
    let data = ctx.request.body
    data.userid = ctx.session.user.id
    data.time = new Date()
    data.can_edit = 0
    let result = null
    if(data.id)
        result = await SchoolData.save(data)
    else
        result = await SchoolData.add(data)
    if(result)
        ctx.body = []
    else
        throw new ApiError(ApiErrorNames.SAVE_SCHOOLDATA_FAILED);
}


exports.list = async (ctx, next) => {
    let usercode = ctx.session.user.code
    let result = null
    result = await SchoolData.list(usercode)
    if(result.length > 0)
        ctx.body = result
    else
        ctx.body = []
}

exports.allarealist = async (ctx, next) => {
    let usercode = ctx.session.user.code
    let result = null
    result = await SchoolData.allarealist(usercode)
    if(result.length > 0)
        ctx.body = result
    else
        ctx.body = []
}

let getUseridById = async (id) => {
    let userid = await SchoolData.getUseridById(id)
    /*console.log(userid)*/
    return userid[0].userid
}

exports.audit = async (ctx, next) => {
    let data = ctx.request.body
    let state = data.state == 1 ? 2 : 3
    data.userid = ctx.session.user.id
    let pulishername = ctx.session.user.name
    data.time = new Date()
    
    await SchoolData.audit(data)
    
    let receiver = await getUseridById(data.school_data_id)
    await NoticeCon.autoMessage(
        {
            title: ('上报数据审核结果-' + (state == 2 ? '通过':'驳回')),
            content: state == 2 ? '您上报的数据已审核通过，无需调整。' : '您上报的数据被驳回，请修改后再次保存。\r\n驳回说明：' + data.content,
            pulisher: data.userid,
            pulishername: pulishername,
            receiver: receiver
    })

    let result = await SchoolData.changestate(state, data.school_data_id)

    if(result)
        ctx.body = []
    else
        throw new ApiError(ApiErrorNames.AUDIT_FAILED);
}

exports.getconfigtime = async (ctx, next) => {
    let userid = ctx.session.user.id
    let result = null
    result = await SchoolData.getconfigtime(userid)
    if(result.length > 0)
        ctx.body = result[0]
    else
        ctx.body = []
}

/* school get time */
exports.gettime = async(ctx, next) => {
    let userid = ctx.session.user.id
    let result = null

    let areauserid = await SchoolData.getareauserid(userid)
    result = await SchoolData.getconfigtime(areauserid)
    if(result.length > 0)
        ctx.body = result[0]
    else
        ctx.body = []
}


exports.postconfigtime = async (ctx, next) => {
    let userid = ctx.session.user.id
    let data = ctx.request.body

    data.userid = userid
    data.areaname = ctx.session.user.name
    data.taskid = 1
    let result = null
    if(data.id)
        result = await SchoolData.saveconfigtime(data)
    else
        result = await SchoolData.addconfigtime(data)
    if(result.length > 0)
        ctx.body = result
    else
        ctx.body = []
}

exports.progress = async (ctx, next) => {
    let userid = ctx.session.user.id
    let usercode = ctx.session.user.code
    let result = null

    let allCount = await SchoolData.allschoolcount(usercode)
    let updateCount = await SchoolData.updatecount(usercode)
    let auditedCount = await SchoolData.auditedcount(usercode)

    let schools = await SchoolData.unupdateschools(usercode)

    result = {
        allCount,
        updateCount,
        auditedCount,
        schools
    }

    if(result)
        ctx.body = result
    else
        ctx.body = []
}

exports.allauditedlist = async (ctx, next) => {

    let usercode = ctx.session.user.code

    let primaryS = await SchoolData.allauditedlist(usercode, 1)
    let juniorS = await SchoolData.allauditedlist(usercode, 2)
    
    let result = {primaryS, juniorS}
    if(result)
        ctx.body = result
    else
        ctx.body = {}
}


exports.schools = async (ctx, next) => {
    let areacode = ctx.session.user.code
    let result = await SchoolData.schools(areacode)

    if(result.length > 0)
        ctx.body = result
    else
        ctx.body = []
}

exports.openEdit = async (ctx, next) => {
    let userid = ctx.request.query.userid
    let L2 = ctx.request.query.name

    let data = {}
    data.userid = userid
    data.can_edit = 1
    data.L2 = L2

    let ids = await SchoolData.getonebyuserid(userid)
    if(ids.length > 0)
        data.id = ids[0]

    console.log(ids)

    let result = null

    if(data.id)
        result = await SchoolData.save(data)
    else
        result = await SchoolData.add(data)

    if(result)
        ctx.body = []
    else
        throw new ApiError(ApiErrorNames.SAVE_SCHOOLDATA_FAILED);
}


exports.exportEXLS = async (ctx, next) => {
    let areacode = ctx.request.query.areacode
    let areaname = ctx.request.query.areaname

    let primaryS = await SchoolData.allauditedlist(areacode, 1)
    let pData = mapEXL_Array(primaryS)

    let juniorS = await SchoolData.allauditedlist(areacode, 2)
    let jData = mapEXL_Array(juniorS)

    let outputData = [{name: '小学', data: pData}, {name: '初中', data: jData}]

    let buffer = exportEXLS(outputData)

    console.log(OUTPUTEXLSPATH)

    let filename = `${areaname}-小学、初中资源配置基本情况-${new Date().getTime()}.xlsx`

    let path = OUTPUTEXLSPATH + filename
    fs.writeFileSync(path, buffer)

    if(filename)
        ctx.body = filename
    else
        ctx.body = []
}

let mapEXL_Array = function (data) {
    let pData = []
    data.map(i => {
        pData.push([i.L2, i.L3, i.L4, i.L5, i.L6, i.L7, i.L8, i.L9, i.L10, i.L11, i.L12, i.L13])
    })
    pData.unshift(['学校名称', '举办者类型', '在校生数', '年级数', '班级数', 
        '每百名学生拥有高于规定学历教师数', '每百名学生拥有县级及以上骨干教师数', 
        '每百名学生拥有体育、艺术（美术、音乐）专任教师数', '生均教学及辅助用房面积（平方米）',
        '生均体育运动场馆面积（平方米）', '生均教学仪器设备值（元）', '每百名学生拥有网络多媒体教室数', '该校综合评估是否达标'])

    return pData
}


let exportEXLS = function (data) {
    let buffer = xlsx.build(data)
    return buffer
} 