var router = require('koa-router')();
var schooldata_controller = require('../../app/controllers/schooldata_controller');

router.post('/save', schooldata_controller.save)
router.get('/getdata', schooldata_controller.one)
router.get('/list', schooldata_controller.list)
router.post('/audit', schooldata_controller.audit)

/* xian/qu area */
router.get('/allarealist', schooldata_controller.allarealist)
router.get('/configtime', schooldata_controller.getconfigtime)
router.post('/configtime', schooldata_controller.postconfigtime)

router.get('/progress', schooldata_controller.progress)
router.get('/allauditedlist', schooldata_controller.allauditedlist)

/* schools */
router.get('/schools', schooldata_controller.schools)

/* excel */
router.get('/exportEXLS', schooldata_controller.exportEXLS)

router.get('/gettime', schooldata_controller.gettime)

router.get('/openEdit', schooldata_controller.openEdit)


module.exports = router;
