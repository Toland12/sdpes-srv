const ApiError      = require('../error/ApiError');
const ApiErrorNames = require('../error/ApiErrorNames');

const Notice        = require('../model/notice_model'); 


exports.announcement = async (ctx, next) => {
    let notice = ctx.request.body
    notice.pulisher = ctx.session.user.id
    notice.time = new Date()
    notice.type = 1
    notice.receiver = ctx.session.user.code
    notice.pulishername = ctx.session.user.name

    let result = null
    if(notice.id)
        result = await Notice.save(notice)
    else
        result = await Notice.add(notice)
    if(result)
        ctx.body = result
    else
        throw new ApiError(ApiErrorNames.SAVE_PEOPLE_FAILED);
}


exports.announcementList = async (ctx, next) => {
    let areacode = ctx.session.user.areacode
    let id = ctx.session.user.id

    let result = await Notice.list(areacode, id)
    if(result.length > 0)
        ctx.body = result
    else
        ctx.body = []
}

exports.message = async (ctx, next) => {
    let notice = ctx.request.body
    notice.pulisher = ctx.session.user.id
    notice.time = new Date()
    notice.type = 2
    notice.pulishername = ctx.session.user.name

    let result = null
    if(notice.id)
        result = await Notice.save(notice)
    else
        result = await Notice.add(notice)
    if(result)
        ctx.body = result
    else
        throw new ApiError(ApiErrorNames.SAVE_PEOPLE_FAILED);
}

exports.autoMessage = async (notice) => {
    notice.time = new Date()
    notice.type = 2
    await Notice.add(notice)
}


exports.messageList = async (ctx, next) => {

    let id = ctx.session.user.id

    let result = await Notice.messageList(id)
    if(result.length > 0)
        ctx.body = result
    else
        ctx.body = []
}

exports.getPushUser = async (ctx, next) => {
    let name = ctx.request.query.name;

    let result = await Notice.getPushUser(name)
    if(result.length > 0)
        ctx.body = result
    else
        ctx.body = []
}
