const db = require('./../db')

const TABLE = 'sd_notice'

const notice = {


  async list (areacode, id) {
    let _sql = `
      SELECT * from sd_notice
      where (LOCATE(receiver, "${areacode}") > 0 OR pulisher = "${id}") AND delete_flag = 0 AND type = 1
    `
    let result = await db.query(_sql)

    return result
  },

  async messageList (id){
    let _sql = `
      SELECT a.*, b.name FROM sd_notice a, sd_users b
      WHERE a.receiver = b.id AND a.type = 2 AND (a.receiver = "${id}" OR a.pulisher = "${id}") AND a.delete_flag = 0
    `
    let result = await db.query(_sql)

    return result
  },

  async add(notice){
    let result = await db.insertData(TABLE, notice)
    return result
  },

  async save(notice){
    let result = await db.updateData(TABLE, notice, notice.id)
    return result
  },

  async getPushUser(name){
    let _sql = `
      SELECT id,code,name FROM sd_users
      WHERE name LIKE "%${name}%" AND delete_flag = 0
    `

    let result = db.query(_sql)
    return result
  }
}

module.exports = notice
