const db = require('./../db')

const TABLE = 'sd_school_data'
const AUDITTABLE = 'sd_audit'
const CONFIGTIMETABLE = 'sd_timeconfig'
const USERTABLE = 'sd_users'

const people = {

  async one (userid) {
    let _sql = `
      SELECT * FROM
      ( SELECT a.id dataid,a.userid,a.taskid,a.can_edit,a.state datastate,
        a.L2,a.L3,a.L4,a.L5,a.L6,a.L7,a.L8,a.L9,a.L10,a.L11,a.L12,a.L13,a.L14,
        a.time datatime,u.\`code\`,u.areacode 
      FROM sd_school_data AS a, sd_users AS u 
      WHERE a.userid = u.id 
      AND a.userid = "${userid}") c
      LEFT JOIN sd_audit AS b ON c.dataid = b.school_data_id 
      AND b.id in (select max(id) from sd_audit group by school_data_id)
    `
    let result = await db.query(_sql)

    return result
  },


  async getonebyuserid (userid) {
    let _sql = `SELECT id FROM sd_school_data WHERE userid=${userid}`
    let result = await db.query(_sql)

    return result
  },

  async changestate(state, id){
    let result = await db.updateData(TABLE, {state: state}, id)
    return result
  },

  async add(data){
    let result = await db.insertData(TABLE, data)
    return result.insertId
  },

  async save(data){
    let result = await db.updateData(TABLE, data, data.id)
    return result
  },

  async getconfigtime(userid){
    let _sql = `
        SELECT * FROM sd_timeconfig WHERE userid = "${userid}" AND delete_flag = 0
    `
    let result = await db.query(_sql)
    return result
  },

  async saveconfigtime(data){
    let result = await db.updateData(CONFIGTIMETABLE, data, data.id)
    return result
  },

  async addconfigtime(data){
    let result = await db.insertData(CONFIGTIMETABLE, data)
    return result.insertId
  },

  async audit(data){
    data.time = new Date()
    let result = await db.insertData(AUDITTABLE, data)
    return result
  },

  async getUseridById(id){
    let _sql = `
      SELECT userid FROM sd_school_data WHERE id = "${id}"
    `
    let result = await db.query(_sql)
    return result
  },

  async list(areacode){
    let _sql = `
      SELECT * FROM
      ( SELECT a.id dataid,a.userid,a.taskid,a.can_edit,a.state datastate,
        a.L2,a.L3,a.L4,a.L5,a.L6,a.L7,a.L8,a.L9,a.L10,a.L11,a.L12,a.L13,a.L14,
        a.time datatime,u.\`code\`,u.areacode 
      FROM sd_school_data AS a, sd_users AS u 
      WHERE a.userid = u.id 
      AND u.areacode = "${areacode}") c 
      LEFT JOIN sd_audit AS b ON c.dataid = b.school_data_id 
      AND b.id in (select max(id) from sd_audit group by school_data_id)
    `
    let result = await db.query(_sql)

    return result
  },

  async allarealist(areacode){
    let _sql = `
      SELECT * FROM
      ( SELECT a.id dataid,a.userid,a.taskid,a.can_edit,a.state datastate,
        a.L2,a.L3,a.L4,a.L5,a.L6,a.L7,a.L8,a.L9,a.L10,a.L11,a.L12,a.L13,a.L14,
        a.time datatime,u.\`code\`,u.areacode,u.areaname
      FROM sd_school_data AS a, sd_users AS u 
      WHERE a.userid = u.id AND u.user_type = 4
      AND u.areacode LIKE "${areacode}%") c
      LEFT JOIN sd_audit AS b ON c.dataid = b.school_data_id 
      AND b.id in (select max(id) from sd_audit group by school_data_id)
    `
    let result = await db.query(_sql)

    return result
  },


  async allschoolcount(usercode){
    let _sql = `SELECT COUNT(*) AS total_count FROM sd_users WHERE delete_flag = 0 AND areacode LIKE '${usercode}%'`
    let result = await db.query(_sql)
    return result[0].total_count
  },

  async updatecount(areacode){
    let _sql = `SELECT COUNT(*) AS total_count FROM
      (SELECT a.*,u.\`code\`,u.areacode,u.areaname
      FROM sd_school_data AS a, sd_users AS u 
      WHERE a.userid = u.id AND a.delete_flag = 0 AND u.user_type = 4
      AND u.areacode LIKE "${areacode}%") c`
    let result = await db.query(_sql)
    return result[0].total_count
  },

  async auditedcount(areacode){
    let _sql = `SELECT COUNT(*) AS total_count FROM
      (SELECT a.*,u.\`code\`,u.areacode,u.areaname
      FROM sd_school_data AS a, sd_users AS u 
      WHERE a.userid = u.id AND a.delete_flag = 0 AND u.user_type = 4 AND a.state = 2
      AND u.areacode LIKE "${areacode}%") c`
    let result = await db.query(_sql)
    return result[0].total_count
  },

  async unupdateschools(areacode){
    let _sql = `
    SELECT areaname, code, id, school_type, name 
      FROM sd_users WHERE id NOT IN (SELECT userid FROM sd_school_data WHERE delete_flag = 0) 
      AND user_type = 4 AND areacode LIKE "${areacode}%" AND delete_flag = 0
    `
    let result = await db.query(_sql)
    return result
  },


  async allauditedlist(areacode, school_type){
    let _sql = `
      SELECT a.id dataid,a.userid,a.taskid,
        a.L2,a.L3,a.L4,a.L5,a.L6,a.L7,a.L8,a.L9,a.L10,a.L11,a.L12,a.L13,
        a.time datatime,u.\`code\`,u.areacode,u.areaname
      FROM sd_school_data AS a, sd_users AS u 
      WHERE a.userid = u.id AND a.state = 2 AND u.school_type = "${school_type}"
      AND u.areacode LIKE "${areacode}%"
    `

    let result = await db.query(_sql)
    return result
  },

  async schools(areacode){
    let _sql = `
      SELECT a.id,a.code,a.name,a.areaname,a.school_type,b.state,b.id data_id,b.can_edit
      FROM sd_users a
      LEFT JOIN sd_school_data b ON a.id = b.userid
      WHERE a.areacode LIKE "${areacode}%" AND a.delete_flag=0 ORDER BY a.id
    `

    let result = await db.query(_sql)
    return result
  },


  async getareauserid(userid){
    let _sql = `SELECT id FROM sd_users WHERE LOCATE(code, 
      (SELECT areacode FROM sd_users WHERE id = "${userid}"))>0  AND user_type=3`

    let result = await db.query(_sql)
    if(result.length > 0)
      return result[0].id
    return ''
  }


}

module.exports = people
